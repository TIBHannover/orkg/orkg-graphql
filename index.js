const { ApolloServer } = require("apollo-server");
const { Neo4jGraphQL } = require("@neo4j/graphql");
const neo4j = require("neo4j-driver");
const { typeDefs } = require("./orkg-schema");
require('dotenv').config();
var rp= require('request-promise');
const { combinedResolvers } = require('./resolvers/resolvers');

const PORT = process.env.PORT || 4000;

const NEO4J_URL      = process.env.NEO4J_URL      || 'bolt://localhost:7687';
const NEO4J_USER     = process.env.NEO4J_USER     || 'neo4j'
const NEO4J_PASSWORD = process.env.NEO4J_PASSWORD || 'neo4j';

// Create a database driver instance
const driver = neo4j.driver(
  NEO4J_URL,
  neo4j.auth.basic(NEO4J_USER, NEO4J_PASSWORD)
);

const schema = new Neo4jGraphQL({
  typeDefs,
  resolvers: combinedResolvers
});

const server = new ApolloServer({
  schema: schema.schema,
  context: { driver },
  // Activate Playground in production environment
  introspection: true,
  playground: {
    settings: {
      'editor.theme': 'light',
    },
  },
});

server.listen(PORT).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Process terminated')
  });
});
