const typeDefs=
`
	type Author {
		name: String
		id: String
        papers: [Paper]
	}


type Resource @exclude(operations: [CREATE, DELETE, UPDATE]) {
    created_by: String
    label: String
    id: String
    created_at: String
    #research_problems: [Resource]
    #details(value: String): [AuditableEntity] @cypher(statement: "MATCH(rr:Paper)-[*]->(this) WITH DISTINCT rr as rrr MATCH(rrr)-[*]->(r:AuditableEntity {label: $value}) RETURN r") #@relation(name: "RELATED", direction: "OUT") #@cypher(statement: "MATCH (this)-[r:RELATED]->(rc:AuditableEntity) WHERE r.predicate_id<>'P32' and r.predicate_id<>'P31' RETURN rc,r")
	#relatedPapers: [Literal] @cypher(statement: "MATCH(rr:Paper)-[*]->(this) MATCH (rr)-[:RELATED {predicate_id: 'P26'}]->(l:Literal) RETURN DISTINCT l") #@cypher(statement: "MATCH(rr:AuditableEntity)-[*]->(this) MATCH(rr)<-[:RELATED {predicate_id: 'P31'}]-(p:Paper) MATCH (p)-[:RELATED {predicate_id: 'P26'}]->(L:Literal) RETURN L")

}

type Paper @exclude(operations: [CREATE, READ, DELETE, UPDATE]) {
    label: String
    id: String
    doi: String @cypher(statement: "MATCH (this)-[:RELATED {predicate_id: 'P26'}]->(r:Literal) RETURN r.label")
    url: String
    authors: [Author] @cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'P27'}]->(rc:AuditableEntity) Optional MATCH (rc)-[:RELATED]->(rr:Literal) RETURN {name: rc.label, orcid: rr.label}")
	totalContributions: Int @cypher(statement: "MATCH(this)-[r:RELATED {predicate_id: 'P31'}]->(rc:Contribution) RETURN COUNT(rc)")
	#details(value: String): [AuditableEntity] @cypher(statement: "MATCH(this)-[*]->(r:AuditableEntity) WHERE r.label CONTAINS $value RETURN DISTINCT r") #@relation(name: "RELATED", direction: "OUT") #@cypher(statement: "MATCH (this)-[r:RELATED]->(rc:AuditableEntity) WHERE r.predicate_id<>'P32' and r.predicate_id<>'P31' RETURN rc,r")
    property: String
    contributions: [ContributionDetail] #@relationship(type: "RELATED", direction: OUT)
}

type Comparison @exclude(operations: [CREATE, DELETE, UPDATE]) {
    created_by: String
    label: String
    id: String
    created_at: String
    doi: String @cypher(statement: "MATCH (this)-[:RELATED {predicate_id: 'P26'}]->(r:Literal) RETURN r.label")
    authors: [AuditableEntity] @cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'P27'}]->(rc:AuditableEntity) RETURN rc") #@relation(name: "RELATED{predicate_id: 'P27'}", direction: OUT)
	totalContributions: Int @cypher(statement: "MATCH(this)-[r:RELATED {predicate_id: 'compareContribution'}]->(rc:Contribution) RETURN COUNT(rc)")
    contributions: [ContributionDetail] #@cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'compareContribution'}]->(rc:Contribution) RETURN rc") #[Contribution] @relation(name: "'RELATED{predicate_id: compareContribution}'", direction: "OUT")
	#relatedPapers(property: String, value: String): [Paper] @cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'compareContribution'}]->(rc:Contribution) MATCH (p:Paper)-[:RELATED {predicate_id: 'P31'}]->(:Contribution {resource_id: rc.resource_id}) RETURN DISTINCT p")
    #relatedPapers(property: String, value: String): [Paper] @cypher(statement: "MATCH(this)-[r:RELATED{predicate_id:'url'}]->(rr:AuditableEntity) RETURN rr")
    #papers(property: String, value: String): [Paper] #@cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'compareContribution'}]->(rc:Contribution) MATCH (p:Paper)-[:RELATED {predicate_id: 'P31'}]->(:Contribution {resource_id: rc.resource_id}) MATCH (this)-[r1:RELATED {predicate_id: 'url'}]->(rr:AuditableEntity) RETURN DISTINCT p")
}

type Problem @exclude(operations: [CREATE, DELETE, UPDATE]) {
    created_by: String
    label: String
    resource_id: String
    created_at: String
	  #relatedPapers: [Paper] @cypher(statement: "MATCH(rr:Paper)-[*]->(this) RETURN DISTINCT rr") #@cypher(statement: "MATCH(rr:AuditableEntity)-[*]->(this) MATCH(rr)<-[:RELATED {predicate_id: 'P31'}]-(p:Paper) MATCH (p)-[:RELATED {predicate_id: 'P26'}]->(L:Literal) RETURN L")
	  relatedPapers(value: String): [Paper] @cypher(statement: "MATCH(n:Paper)-[r:RELATED {predicate_id: 'P31'}]->(c:Contribution) MATCH(c)-[*]->(rr:AuditableEntity) WHERE rr.label=$value RETURN n")
	  relatedComparisons: [Comparison] @cypher(statement: "MATCH(this)<-[:RELATED]-(rr:Contribution) MATCH(rr)<-[:RELATED]-(r:Comparison) RETURN DISTINCT r")
	  papers: [Paper] @cypher(statement: "MATCH(n:Paper)-[r:RELATED {predicate_id: 'P31'}]->(c:Contribution) MATCH(c)-[*]->(this) RETURN DISTINCT n")

}

type Literal @exclude(operations: [CREATE, DELETE, UPDATE]) {
    label: String
    datatype: String
    created_at: String
    created_by: String
    labels: String
    literal_id: String
    paper: Paper @relationship(type: "RELATED", direction: IN)
	  details(value: String): AuditableEntity @cypher(statement: "MATCH(this)<-[:RELATED]-(p:Paper)-[*]->(r:AuditableEntity) WHERE r.label CONTAINS $value RETURN DISTINCT r")
}

type Predicate @exclude(operations: [CREATE, DELETE, UPDATE]) {
    label: String
    created_by: String
    predicate_id: String
    created_at: String
}

type ContributionDetail {
  id: String
  label: String
  paper: Paper
  predicateDetails: PredicateDetails
  data: [DataObject]
}

type PredicateDetails {
  property: String
  label: String
}

type DataObject {
  propertyId: String
  propertyLabel: String
  label: String
  subComponents: [DataObject]
}

input FilterCondition {
  AND: [FilterCondition!]
  OR: [FilterCondition!]
  property: String
  value: String
  _LT: Float
  _LTE: Float
  _GT: Float
  _GTE: Float
  _EQ: Float
  _NEQ: Float
}

type AuditableEntity @exclude(operations: [CREATE, DELETE, UPDATE]) {
    created_by: String
    label: String
    resource_id: String
    created_at: String
    property: String
    literal: String
    id: AuditableEntity @cypher(statement: "MATCH (this)-[r:RELATED {predicate_id: 'HAS_ORCID'}]->(rc:AuditableEntity) RETURN rc") #@relation(name: "RELATED{predicate_id: 'HAS_ORCID'}", direction: "OUT")
	  #resource_details: AuditableEntity @relationship(type: "RELATED", direction: OUT) #@cypher(statement: "MATCH (this)-[r:RELATED]->(rc:AuditableEntity) WHERE r.predicate_id<>'P32' and r.predicate_id<>'P31' RETURN rc,r")
	  #details(avg: Float): [AuditableEntity] @cypher(statement: "MATCH (this)-[r:RELATED]->(rc:AuditableEntity) WHERE r.predicate_id<>'P32' and r.predicate_id<>'P31' RETURN rc,r")
	  #relatedPapers: Literal @cypher(statement: "MATCH(rr:AuditableEntity)-[*]->(this) MATCH(rr)<-[:RELATED {predicate_id: 'P31'}]-(p:Paper) MATCH (p)-[:RELATED {predicate_id: 'P26'}]->(L:Literal) RETURN DISTINCT L")
	  #average: Float @cypher(statement: "MATCH (this)-[:RELATED]->(r:AuditableEntity) WITH avg(toFloat(r.label)) as average RETURN average")
}


type Query {
  author(id: String, where: [FilterCondition]): Author #@cypher(statement: "MATCH(n:Literal {label: $id})<-[:RELATED]-(r:Resource) RETURN { name: r.label, id: n.label }")
  Resource(label: String, resource_id: String, relatedPapers: String): [Resource]
  #Paper(organization_id: String, observatory_id: String, extraction_method: String, verified: String, created_by: String, label: String, resource_id: String, created_at: String, doi: Literal): [Resource]
  Paper(doi: String): Paper #@cypher(statement: "MATCH (n:Paper)-[:RELATED {predicate_id: 'P26'}]->(:Literal {label: $doi}) RETURN DISTINCT n")
  papers(dois: [String], field: [String], where: [FilterCondition]): [Paper]
  Literal(label: String, datatype: String, literal_id: String): [Literal]
  Problem(label: String, resource_id: String): [Problem]
  comparison(doi: String, where: [FilterCondition]): Comparison @cypher(statement: "MATCH (n:Comparison)-[:RELATED {predicate_id: 'P26'}]->(:Literal {label: $doi}) RETURN DISTINCT n")
  findResourceByPropertyLabel(label: String) : [Resource] @cypher(statement: "MATCH(n:Predicate {label: $label}) MATCH(rr:Resource)<-[:RELATED {predicate_id: n.predicate_id}]-(r:AuditableEntity) RETURN rr")
  findPapersByProblem(value: String): [Resource] @cypher(statement: "MATCH(n:Paper)-[r:RELATED {predicate_id: 'P31'}]->(c:Contribution) MATCH(c)-[*]->(rr:AuditableEntity) MATCH(c)-[*]->(rrr:Resource) WHERE rr.label=$value RETURN rrr")

}
`;

module.exports = {typeDefs};
