# syntax=docker/dockerfile:1

FROM node:current-alpine

ENV NODE_ENV=production

EXPOSE 4000

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm ci

COPY . .

CMD ["node", "index.js"]
