# ORKG GraphQL

This is the repository belongs to GraphQL endpoint for the Open Research Knowledge Graph (ORKG)

### Installation

Clone this repository:

    git clone https://gitlab.com/TIBHannover/orkg/orkg-graphql.git

Go to the ORKG GraphQL directory:

    cd orkg-graphql

Install the dependencies by running:

    npm install

If you need additional configuration, create .env file and add the following variables (the values listed are the default values):

```ini
NEO4J_URL=bolt://localhost:7687
NEO4J_USER=neo4j
NEO4J_PASSWORD=neo4j
```

### Backend service

In order to run the GraphQL endpoint, the Neo4j databases service needs to be running as well through the backend service. Please refer to the [ORKG backend repository](https://gitlab.com/TIBHannover/orkg/orkg-backend) for instructions on how to run the backend.

## Running

### Development

Run the following command:

    npm run start

Open the browser and enter the URL of the application: http://localhost:4000/.

### Docker

The project provides configuration to run the service with Docker Compose. It is set up so that it works with the services from the `orkg-backend` repository without additional configuration.

# Contributing

Please feel free to contribute to GraphQL endpoint. In case you confront with any bugs, please [open an issue](https://gitlab.com/TIBHannover/orkg/orkg-graphql/issues).


## Examples

### Querying with sandbox

Find a paper and its contributions using the DOI:

```graphql
{
  Paper(doi: "10.1109/TITS.2013.2296697") {
    label, id {
      label
    }
    contributions {
      label
    }
  }
}
```

Find all papers related to a research problem:

```graphql
{
  Resource(label: "Determination of the COVID-19 basic reproduction number") {
    label, relatedPapers {
      label
    }
  }
}
```

### Querying with curl

To query with `curl`, a simple JSON payload needs to be constructed.
A translation of the first query in the earlier subsection will look like this:

```sh
curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{ "query": "{ Paper(doi: \"10.1109/TITS.2013.2296697\") { label, id { label } contributions { label } } }" }' \
     http://localhost:4000
```

NOTE: Double-quotes in the GraphQL query need to be escaped properly in the JSON payload to make the query work.
