const { fetchContributions } = require('./Contribution');

const PapersResolver = {
  papers: async (parent, { dois, field, where }, context, info) => {
    let session = context.driver.session();

    let query = '';
    if (field!=undefined && field.length > 0) {
      query = `
      MATCH (p:Paper)-[:RELATED {predicate_id: 'P26'}]->(l:Literal)
      WHERE ${field.map(f => `p.label CONTAINS "${f}"`).join(' OR ')}
      RETURN p, l.label AS Doi
    `;
    }

  if (dois && dois.length>0) {
      let arrangeDois = '[' + dois.map(doi => `'${doi.replace(/'/g, "\\'")}'`).join(', ') + ']';
      query = `MATCH (p:Paper)-[:RELATED {predicate_id: 'P26'}]->(l:Literal)
               WHERE l.label IN ${arrangeDois}
               RETURN DISTINCT p, l.label AS Doi`;
    }

    console.log(query);
    let responses = [];
    try {
      const result = await session.run(query);

      responses = await Promise.all(result.records.map(async record => {
      const p = record.get('p');
      const contributions = await fetchContributions(parent, { id: p.properties.id, where }, context, info);
      if (contributions.length > 0) {
        console.log('doi', record.get('Doi'));
        return {
          label: p.properties.label,
          id: p.properties.id,
          doi: record.get('Doi'),
          contributions: contributions,
        };
      } else {
        return null; // Return null for records without contributions
      }
    }));

    return responses.filter(response => response !== null);
  } catch (error) {
    console.error('Error fetching records:', error);
    return []; // Return an empty array or handle errors as appropriate
  }
    finally {
      session.close();
    };

    return responses;
}
}

module.exports = { PapersResolver };
