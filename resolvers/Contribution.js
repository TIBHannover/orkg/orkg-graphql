const { filterContributions } = require('./FilterPapers')

  async function fetchContributions(paper, args, context, info) {
    let session = context.driver.session();

    let id = '';
    if ('id' in args) {
      id = args.id
    }

    let where = ''
    if ('where' in args && (args.where !=undefined || args.where!='')) {
      where = args.where
    }



    const contributions = await session.run(`MATCH(Paper {id: $paperId})-[r:RELATED {predicate_id: 'P31'}]->(rc:Contribution)
        RETURN rc.id as id, rc.label as label`, {paperId: id});
      session.close();

    let res = [];

      await Promise.all(contributions.records.map(async c => {
        const newSession = context.driver.session()
        const id = c._fields[0];
        const label = c._fields[1];
        let result = [];
        try {
          result = await newSession.run(
            // Adjust the Cypher query to fetch contributions by the paper ID
            `MATCH path=(Contribution {id: $contributionId})-[*1..10]->(n)
            UNWIND relationships(path) AS r
            OPTIONAL MATCH (p:Predicate) WHERE p.id = r.predicate_id
            WITH path, collect({id: r.predicate_id, label: p.label}) AS propertyDetails
            RETURN path, propertyDetails`,
            { contributionId: id }
          );

          const data = await extractData(result.records, id, where);
          if (data.length > 0) {
            res.push({id, label, data});
        }
        } catch (error) {
          console.error(error);
        } finally {
          await newSession.close();
      }
      }));

      return res;
  }

const extractData = async (response, id, where) => {
  let properties = [];
  const idLabelMap = {};
  const parsedData = response.map(record => {
  // Assuming the first item in _fields is the one containing the segments
  //console.log(record._fields[1])

  record._fields[1].forEach(item => {
    if (!(item.id in idLabelMap)) {
      idLabelMap[item.id] = item.label;
    }
  });

  const segments = record._fields[0].segments;
  return segments.map(segment => {
    return {
      startNode: {
        labels: segment.start.properties.label,
        properties: segment.start.properties.id
      },
      relationship: {
        properties: segment.relationship.properties.predicate_id
      },
      endNode: {
        labels: segment.end.properties.label,
        properties: segment.end.properties.id
      }
    }
  });
}).flat();

  //console.log(idLabelMap)
  //console.log(idLabelMap['P32'])

  const uniqueSegments = removeDuplicateEndNodes(parsedData);
  

  uniqueSegments.forEach(item => {
  if (item.relationship && item.relationship.properties) {
    const id = item.relationship.properties;
    const label = idLabelMap[id]; // Lookup the label using the id

    // Update the relationship object with propertyId and propertyLabel
    item.relationship = {
      propertyId: id,
      propertyLabel: label
    };
  }
});

  //console.log(uniqueSegments)

  let search = true;
  if (where && (where !== '' || where!=undefined)) {
    search = await filterContributions(uniqueSegments, where)
  }

  if (search) {
    const startNodeId = id;
    const processedData = processData(uniqueSegments, startNodeId)
    return processedData;
  } else {
    return [];
  }

}

function removeDuplicateEndNodes(segments) {
  const uniqueEndNodeLabels = new Set();

  // Filter the array to keep only segments with a unique endNode label
  const uniqueSegments = segments.filter(segment => {
    const label = segment.endNode.labels;
    if (!uniqueEndNodeLabels.has(label)) {
      uniqueEndNodeLabels.add(label);
      return true;
    }
    // If we've seen this endNode label before, filter out this segment
    return false;
  });

  return uniqueSegments;
}

function parseDataObject(object) {
  return {
    label: object.label,
    resourceId: object.id, // Replace with actual resource ID field if different
    description: object.description, // Replace with actual description field if different
    subComponents: object.subComponents ? object.subComponents.map(parseDataObject) : []
  };
}

function processData(segments, startNodeId, maxDepth = 10, currentDepth = 0) {
  // Base case: if we've reached the maximum depth, stop recursing
  if (currentDepth >= maxDepth) {
    return [];
  }

  // Filter segments based on the current startNodeId
  const filteredSegments = segments.filter(segment => segment.startNode.properties === startNodeId);

  // Map over the filtered segments to transform them
  const data = filteredSegments.map(segment => {
    return {
      label: segment.endNode.labels,
      //property: segment.endNode.properties,
      propertyId: segment.relationship.propertyId,
      propertyLabel: segment.relationship.propertyLabel,
      // Recursively process the next level of nodes, using the current endNode as the next startNode
      subComponents: processData(segments, segment.endNode.properties, maxDepth, currentDepth + 1).flat()
    };
  });

  return data;
}

module.exports = { fetchContributions };
