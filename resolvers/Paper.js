const { fetchContributions } = require('./Contribution');

const PaperResolver = {
  Paper: async (parent, { doi }, context, info) => {
    let session = context.driver.session();
    
    const query = `MATCH (n:Paper)-[:RELATED {predicate_id: 'P26'}]->(:Literal {label: $doi}) RETURN DISTINCT n`;
    let response = {};
    try {
      const result = await session.run(query, {doi});
      
      if (result.records.length > 0) {
        const id = result.records[0]._fields[0].properties.id;
       response = {
          label: result.records[0]._fields[0].properties.label,
          id,
          contributions: await fetchContributions(parent, { id }, context, info),
        };
      }
    }
    catch(error) {
      console.error('Error:', error);
    }
    finally {
      session.close(); // Close the session
    };

    return response;
}
}

module.exports = { PaperResolver };
