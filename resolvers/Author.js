const { fetchContributions } = require('./Contribution');

const AuthorResolver = {
  author: async (parent, { id, where }, context, info) => {
    const session = context.driver.session();

    const query = `MATCH 
        (n:Literal {label: $id})<-[:RELATED {predicate_id: 'HAS_ORCID'}]-(rc:Resource),
        (rc)<-[:RELATED {predicate_id: 'hasListElement'}]-(list:Resource),
        (list)<-[:RELATED {predicate_id: 'hasAuthors'}]-(paper:Resource)-[:RELATED {predicate_id: 'P26'}]->(l:Literal)
        RETURN 
          rc.label AS author, 
          COLLECT(paper) AS paper, l.label AS doi`;

      let response = [];
      let authorName = '';
      try {
        const result = await session.run(query, {id});
        if (result.records.length > 0) {
          console.log(result.records[0]._fields[2]);
          authorName = result.records[0]._fields[0];
          response = result.records[0]._fields[1].map(async r => {
          const contributions = await fetchContributions(parent, { id: r.properties.id, where } , context, info);
          if (contributions.length > 0) {
            return {
              id: r.properties.id,
              doi: result.records[0]._fields[2],
              label: r.properties.label,
              contributions: contributions,
            }
        } else {
          return null;
        }
        });

        }
      } catch (error) {
          console.error(error);
        } finally {
          await session.close();
      }
    response = await Promise.all(response);
    response = response.filter(item => item !== null);
    return {id, name: authorName, papers: response };
}
}

module.exports = { AuthorResolver };
