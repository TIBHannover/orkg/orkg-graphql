

  async function filterContributions(contributions, where) {
    console.log(where);
    let response = false;
  for (const c of contributions) {
    if (searchInWhere(c, where)) {
      response = true;
      break; // Exit the loop as soon as searchInWhere returns true
    }
  }
  return response;
}


// fix if all the conditions are true then return the true
function searchInWhere(c, where) {
  return where.some(condition => {
    // Check if the property matches
    if (c.relationship.propertyId === condition.property) {
      if (applyFilter(c.startNode.labels, condition) || (c.endNode && applyFilter(c.endNode.labels, condition))) {
        return true;
      }
    }
    // Return false explicitly for clarity, though it's not strictly necessary
    return false;
  });
}


function applyFilter(valueString, condition) {
    // Numeric comparisons: Check if a numeric condition is explicitly stated
    if ('_LT' in condition || '_LTE' in condition || '_GT' in condition || '_GTE' in condition || 'EQ' in condition || 'NEQ' in condition) {
        const value = parseFloat(valueString);
        if (isNaN(value)) {
            return false; // If conversion fails and a numeric comparison is expected, return false
        }
        
        if (condition._LT !== undefined && !(value < condition._LT)) return false;
        if (condition._LTE !== undefined && !(value <= condition._LTE)) return false;
        if (condition._GT !== undefined && !(value > condition._GT)) return false;
        if (condition._GTE !== undefined && !(value >= condition._GTE)) return false;
        if (condition._EQ !== undefined && !(value === condition._EQ)) return false;
        if (condition._NEQ !== undefined && !(value !== condition._NEQ)) return false;
    }

    if (condition.value !== undefined) {
        const lowerValueString = valueString.toLowerCase();
        const lowerConditionValue = condition.value.toLowerCase();
        // Check if valueString contains condition.value or if condition.value contains valueString
        if (!(lowerValueString.includes(lowerConditionValue) || lowerConditionValue.includes(lowerValueString))) {
            return false; // If neither string contains the other, return false
        }
    }

    return true; // Return true if none of the conditions are violated
}

module.exports = { filterContributions };
