const { ComparisonResolver } = require('./Comparison');
const { PapersResolver } = require('./Papers');
const { AuthorResolver } = require('./Author');
const { PaperResolver } = require('./Paper');

const combinedResolvers = ({
  Comparison: ComparisonResolver,
  //Paper: ContributionResolver,
  Query: {
    ...PaperResolver,
    ...PapersResolver,
    ...AuthorResolver,
  }
  //Author: PapersResolver,
});

module.exports = { combinedResolvers };
