
function getUrlWithId(id) {
  return `https://orkg.org/simcomp/thing/?thing_type=COMPARISON&thing_key=${id}`;
}

async function getPaperInfo(id, context) {
  const session = context.driver.session();
        try {
          const result = await session.run(
          `MATCH (n: Paper)-[r1:RELATED {predicate_id: 'P31'}]->(rc:Contribution {id: $contribution_id}),
          (n)-[r2:RELATED {predicate_id: 'P26'}]->(l:Literal)
          RETURN n.id AS id, n.label AS label, l.label AS doi`, { contribution_id: id });
          //console.log(result.records[0]._fields[2]);
          const res = result.records[0];
          //console.log(res._fields[0]);
          return {id: res._fields[0], label: res._fields[1], doi: res._fields[2]}

    } catch (error) {
      console.error('Error fetching paper information:', error);
    } finally {
      await session.close();
    }
}

const ComparisonResolver = {

  async contributions(parent, args, context, info) {
        let res = [];
        let resource_ids = [];
        let ids = '';
        let comparisonData = [];
        let contributions = [];
        let node = '';
        

        const url = getUrlWithId(parent.id);

        await fetch(url)
          .then(response => response.json())
          .then(async data => {
            //console.log(data); // Handle the fetched data

            let comparisonContributions = data['payload']['thing']['data'];
            //console.log(comparisonContributions)
            let properties = comparisonContributions['predicates'];
            //console.log(properties)
            let values = comparisonContributions['data'];
            //console.log(properties)

            contributions = await comparisonContributions['contributions'].map(async contribution => {
              // Initialize the contribution object with basic information
              comparisonData[contribution.id]=[]
              const paperInfo = await getPaperInfo(contribution.id, context);
              console.log(paperInfo);
              
              Object.keys(values).map(function(key, index) {
                for(let k=0; k<values[key].length; k++) {
                  if(values[key][k][0]['path'] && values[key][k][0]['path'].indexOf(contribution.id) !== -1) {
                    let property = properties.find(p => p['id']===key);
                    comparisonData[contribution.id].push({propertyId: property['id'], propertyLabel: property['label'], label: values[key][k][0]['label']});
                    //comparisonData[c['id']].push({paperId: c['paperId'], title: c['title']});
                  }
                }
                //console.log(comparisonData);
            });

              const contributionObj = {
                    id: contribution.id,
                    label: contribution.label,
                    paper: { id: paperInfo?.id, label: paperInfo?.label, doi: paperInfo?.doi },
                    data: comparisonData[contribution.id]
              };

              return contributionObj;
          });
            return contributions;
        })
          .catch(error => {
          console.error('Error fetching data:', error);
      });

        return contributions;
  }
};

module.exports = { ComparisonResolver };
