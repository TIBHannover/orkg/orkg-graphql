# Changelog

All changes to the ORKG GraphQL endpoint will be documented in this file. The format is based on [Keep a
Changelog](https://keepachangelog.com/en/1.0.0/) and we adhere to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

---
## V0.1 - 2021-05-03

### Changes

- Implemented basic data fetch using GraphQL.
